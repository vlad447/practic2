﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace MathLib
{
    public class ClassPractic2
    {
        #region --- Исходные данные

        public ClassPractic2()
        /// Координаты у, м
        {
            SloyM = new double[] { 0, 0.5, 1.0, 1.5, 2.0, 2.5, 3.0, 3.5, 4.0 };
        }
        /// Высота слоя H0, м
        private double _H0;
        public double H0
        {
            get { return _H0; }
            set { _H0 = value; }
        }
        /// Начальная температура материала t', 0С
        private double _T0Mat;
        public double T0Mat
        {
            get { return _T0Mat; }
            set { _T0Mat = value; }
        }
        /// Начальная температура газа t', 0С
        private double _T0Gaz;
        public double T0Gaz
        {
            get { return _T0Gaz; }
            set { _T0Gaz = value; }
        }
        /// Скорость газа на свободное сечение шахты Wг, м/с
        private double _VoGaz;
        public double VoGaz
        {
            get { return _VoGaz; }
            set { _VoGaz = value; }
        }
        /// Средняя теплоемкость газа Cг, кДж/(м3 • К)
        private double _Cg;
        public double Cg
        {
            get { return _Cg; }
            set { _Cg = value; }
        }
        ///  Расход материалов,Cм кг/с
        private double _Cm;
        public double Cm
        {
            get { return Math.Round(_Cm, 3); }
            set { _Cm = value; }
        }
        ///  Теплоемкость материалов Gм, кДж/(кг • К)
        private double _Gm;
        public double Gm
        {
            get { return Math.Round(_Gm, 3); }
            set { _Gm = value; }
        }
        /// Объемный коэффициент теплоотдачи av, Вт/(м3 • К).
        private double _aV;
        public double aV
        {
            get { return _aV; }
            set { _aV = value; }
        }

        /// Диаметр аппарата, м 
        private double _D;
        public double D
        {
            get { return _D; }
            set { _D = value; }
        }

        #endregion --- Исходные данные

        #region --- Расчетные показатели

        /// Площад, м2
        public double S()
        {
            double _S = Math.PI * Math.Pow(D / 2, 2);
            return Math.Round(_S, 3);
        }


        ///Отношение теплоемкостей потоков
        public double m()
        {
            double _ОtnPotokov = (Cm * Gm) / (S() * VoGaz * Cg);
            return Math.Round(_ОtnPotokov, 3);
        }
        /// Полная относительная высота слоя 
        public double Y0()
        {
            double _HOtnos = (aV * S() * H0) / (1000 * Cg * VoGaz * S());
            return Math.Round(_HOtnos, 3);
        }


        /// Координаты у, м
        public double[] SloyM { get; set; } = new double[] { 0, 0.5, 1.0, 1.5, 2.0, 2.5, 3.0, 3.5, 4.0 };

        ///Высота Y
        public double[] Y { get; set; }
        ///1-exp(Y)
        public double[] Exp { get; set; }
        ///1-mexp(Y)
        public double[] MExp { get; set; }
        ///Значение V
        public double[] Vr { get; set; }
        ///Значение Тетта
        public double[] v_tet { get; set; }
        /// t 1
        public double[] Ttet { get; set; }
        /// t 2
        public double[] t_tet { get; set; }
        ///Разность температур, °С
        public double[] Traznost { get; set; }

        public double[] getY()
        {
            Y = new double[SloyM.Length];
            for (int i = 0; i < Y.Length; i++)
            {
                Y[i] = Math.Round(aV * SloyM[i] / (1000 * Cg * VoGaz), 3);
            }

            return Y;
        }

        public double MExpY0()
        {
            return Math.Round(1 - m() * Math.Exp(-(1 - m()) * Y0() / m()), 3);
        }

        public double[] GetExp()
        {
            getY();

            Exp = new double[SloyM.Length];
            for (int i = 0; i < Exp.Length; i++)
            {
                Exp[i] = Math.Round(1 - Math.Exp((m() - 1) * Y[i] / m()), 3);
            }

            return Exp;
        }

        public double[] GetMExp()
        {
            GetExp();

            MExp = new double[SloyM.Length];
            for (int i = 0; i < Exp.Length; i++)
            {
                MExp[i] = Math.Round(1 - m() * Math.Exp((m() - 1) * Y[i] / m()), 3);
            }

            return MExp;
        }

        public double[] GetVr()
        {
            GetMExp();

            Vr = new double[SloyM.Length];
            for (int i = 0; i < Exp.Length; i++)
            {
                Vr[i] = Math.Round(Exp[i] / MExpY0(), 3);
            }

            return Vr;
        }

        public double[] Get_v_tet()
        {
            GetVr();

            v_tet = new double[SloyM.Length];
            for (int i = 0; i < Exp.Length; i++)
            {
                v_tet[i] = Math.Round(MExp[i] / MExpY0(), 3);
            }

            return v_tet;
        }

        public double[] Get_t_tet()
        {
            Get_v_tet();

            t_tet = new double[SloyM.Length];
            for (int i = 0; i < Exp.Length; i++)
            {
                t_tet[i] = Math.Round(T0Mat + (T0Gaz - T0Mat) * Vr[i], 3);
            }

            return t_tet;
        }

        public double[] GetTtet()
        {
            Get_t_tet();

            Ttet = new double[SloyM.Length];
            for (int i = 0; i < Exp.Length; i++)
            {
                Ttet[i] = Math.Round(T0Mat + (T0Gaz - T0Mat) * v_tet[i], 3);
            }

            return Ttet;
        }

        public double[] GetTraznost()
        {
            GetTtet();

            Traznost = new double[SloyM.Length];
            for (int i = 0; i < Exp.Length; i++)
            {
                Traznost[i] = Math.Round(t_tet[i] - Ttet[i], 3);
            }

            return Traznost;
        }

        public void Raschet()
        {
            GetTraznost();
        }



        #endregion --- Расчетные показатели

    }

}
