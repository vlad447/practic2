﻿using MathLib;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace _2021_ZubakovSemon.Models
{
    public class DataOutputNewModel
    {
        private ClassPractic2 _stenka = new ClassPractic2();

        private DataInputNewModel _DataInput = new DataInputNewModel();
        public ClassPractic2 StenkaData { get { return _stenka; } }

        public DataOutputNewModel()
        {

        }
        public DataOutputNewModel(DataInputNewModel DataInput):base()
        {
            _DataInput = DataInput;

            #region --- Передать исходные данные в экземпляр библиотеки

            _stenka.H0 = _DataInput.H0;
            _stenka.T0Mat = _DataInput.T0Mat;
            _stenka.T0Gaz = _DataInput.T0Gaz;
            _stenka.VoGaz = _DataInput.VoGaz;
            _stenka.Cg = _DataInput.Cg;
            _stenka.Cm = _DataInput.Cm;
            _stenka.Gm = _DataInput.Gm;
            _stenka.aV = _DataInput.aV;
            _stenka.D = _DataInput.D;

            #endregion --- Передать исходные данные в экземпляр библиотеки

            Raschet();
        }



        #region --- Получить расчетные показатели
        public double[] SloyM { get { return _stenka.SloyM; } }
        public double[] Y { get; set; }
        public double[] Exp { get; set; }
        public double[] MExp { get; set; }

        [JsonPropertyName("_Vr")]
        public double[] Vr { get; set; }
        [JsonPropertyName("v_tet")]
        public double[] v_tet { get; set; }
        [JsonPropertyName("_Ttet")]
        public double[] Ttet { get; set; }
        [JsonPropertyName("t_tet")]
        public double[] t_tet { get; set; }
        public double[] Traznost { get; set; }

        public void Raschet()
        {
            _stenka.Raschet();

            Y = _stenka.Y;
            Exp = _stenka.Exp;
            MExp = _stenka.MExp;
            Vr = _stenka.Vr;
            v_tet = _stenka.v_tet;
            Ttet = _stenka.Ttet;
            t_tet = _stenka.t_tet;
            Traznost = _stenka.Traznost;
        }

        #endregion --- Получить расчетные показатели

    }

    public class ArvicheOutputs
    {
        public DataInputNewModel[] DataSet { get; set; }
    }
}
