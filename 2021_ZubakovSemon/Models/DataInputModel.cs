﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace _2021_ZubakovSemon.Models
{
    public class DataInputNewModel
    {
        [LiteDB.BsonId(true)]
        public int ID { get; set; } = 0;

        public double[] SloyM { get; set; }

       
        /// Высота слоя H0, м
        public double H0 { get; set; }

        /// Начальная температура материала t', 0С
        public double T0Mat { get; set; }

        /// Начальная температура газа t', 0С
        public double T0Gaz { get; set; }

        /// Скорость газа на свободное сечение шахты Wг, м/с
        public double VoGaz { get; set; }

        /// Средняя теплоемкость газа Cг, кДж/(м3 • К).
        public double Cg { get; set; }

        ///  Расход материалов,Cм кг/с
        public double Cm { get; set; }

        ///  Теплоемкость материалов Gм, кДж/(кг • К)
        public double Gm { get; set; }

        /// Объемный коэффициент теплоотдачи av, Вт/(м3 • К).
        public double aV { get; set; }

        /// <summary>
        /// Диаметр аппарата, м 
        /// </summary>
        public double D { get; set; }
        public string Email { get; set; }
        public string Name { get; set; }

        public bool IsSaved { get; set; }
        public string ActionSave { get; set; }

    }
}
