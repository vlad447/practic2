﻿using _2021_ZubakovSemon.Models;
using FastReport.Web;
using MathLib;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Wkhtmltopdf.NetCore;

namespace _2021_ZubakovSemon.Controllers
{
    public class HomeController : Controller
    {
        readonly ILiteDbContext _myContext;

        readonly IGeneratePdf _generatePdf;
        readonly IHostEnvironment _hostingEnvironment;

        private readonly ILogger<HomeController> _logger;

        public HomeController(ILogger<HomeController> logger, IHostEnvironment hostingEnvironment, ILiteDbContext myContext)
        {
            _logger = logger;
            _myContext = myContext;
        }


        public IActionResult Index()
        {
            return View();
        }
        public IActionResult Contact()
        {
            return View();
        }

        public IActionResult InputNew()
        {
            ClassPractic2 _stenka = new ClassPractic2();

            #region --- Задать исходные данные по умолчанию

            _stenka.H0 = 4;
            _stenka.T0Mat = 650;
            _stenka.T0Gaz = 10;
            _stenka.VoGaz = 0.78;
            _stenka.Cg = 1.31;
            _stenka.Cm = 1.72;
            _stenka.Gm = 1.49;
            _stenka.aV = 2460;
            _stenka.D = 2.1;

            DataInputNewModel _DataInput = new DataInputNewModel();

            _DataInput.H0 = _stenka.H0;
            _DataInput.T0Mat = _stenka.T0Mat;
            _DataInput.T0Gaz = _stenka.T0Gaz;
            _DataInput.VoGaz = _stenka.VoGaz;
            _DataInput.Cg = _stenka.Cg;
            _DataInput.Cm = _stenka.Cm;
            _DataInput.Gm = _stenka.Gm;
            _DataInput.aV = _stenka.aV;
            _DataInput.D = _stenka.D;
            _DataInput.SloyM = _stenka.SloyM;


            #endregion --- Задать исходные данные по умолчанию


            return View(_DataInput);
        }

        [HttpPost]
        public IActionResult DataInputNew(DataInputNewModel DataInput)
        {
            HttpContext.Session.Set<DataInputNewModel>("IntputNew", DataInput);

            if (DataInput.ActionSave=="true")
            {
                return SaveData(DataInput);
            }

            DataOutputNewModel Raschet = new DataOutputNewModel(DataInput);
            Raschet.Raschet();

            HttpContext.Session.Set<DataOutputNewModel>("OutputNew", Raschet);


            ViewBag.SuccessSave = false;

            return View("OutputNew", Raschet);
        }

        public IActionResult GraphicNew()
        {
            if (HttpContext.Session.Keys.Contains("OutputNew"))
            {
                DataOutputNewModel Raschet = HttpContext.Session.Get<DataOutputNewModel>("OutputNew");

                GraphicNewModel model = new GraphicNewModel(Raschet.StenkaData);
                return View(model);
            }
            return View();
        }

        [HttpGet]
        public IActionResult InputFromStore(int id)
        {
            var data = _myContext.GetRaschet(id);
            data.ActionSave = "false";
            return View("InputNew", data);
        }

        public IActionResult Create()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        [HttpPost]
        public ActionResult SaveData(DataInputNewModel model)
        {
            try
            {
                DataInputNewModel Data = HttpContext.Session.Get<DataInputNewModel>("InputNew");
                
                if(Data==null)
                {
                    Data = model;
                    _myContext.SaveRaschet(new DataInputNewModel[] { Data });
                    HttpContext.Session.Set<DataInputNewModel>("InputNew", Data);
                    Data.IsSaved = true;
                }
                else if (!Data.IsSaved)
                {
                    _myContext.SaveRaschet(new DataInputNewModel[] { Data });
                    Data.IsSaved = true;
                }
                return View("InputNew", Data);
            }
            catch (Exception er)
            {
                return BadRequest(); 
            }
        }
            
        [HttpGet]
        public ActionResult Archive(string email)
        {
            try
            {
                ArvicheOutputs data = new ArvicheOutputs();
                data.DataSet = _myContext.GetRaschet(email);
                return View(data);
            }
            catch (Exception er)
            {
                return BadRequest();
            }
        }

    }
}
